package com.classpath.loansmicroservice.util;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.classpath.loansmicroservice.model.BalanceInfo;
import com.classpath.loansmicroservice.model.Notification;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoanUtilities {

	private static RestTemplate restTemplate = new RestTemplate();
	private final static String NOTIFICATION_API = "http://104.198.54.20:8080/api/notifications/customer/";
	private final static String BALANCE_INFO_API = "http://35.188.88.103:8081/api/account/";

	public static ResponseEntity<BalanceInfo> getBalanceInfo(int accountId) {
		ResponseEntity<BalanceInfo> customerResponseEntity = null;
		try {
			customerResponseEntity = restTemplate.getForEntity(BALANCE_INFO_API + accountId, BalanceInfo.class);
			log.info("Response from account microservice - customer deatails :: {}", customerResponseEntity.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customerResponseEntity;
	}

	public static void sendNotification(Notification notification) {

		try {
			final ResponseEntity<Notification> responseEntity = restTemplate
					.postForEntity(NOTIFICATION_API + notification.getCustomerid(), notification, Notification.class);
			log.info("Response from notification microservice :: {}", responseEntity.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
