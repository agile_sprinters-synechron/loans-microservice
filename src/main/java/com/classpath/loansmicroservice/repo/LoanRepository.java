package com.classpath.loansmicroservice.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.classpath.loansmicroservice.model.Loan;


@Repository
public interface LoanRepository extends CrudRepository<Loan, Integer>{

	public List<Loan> findByCustomerId(int customerId);

	public Loan findByCustomerIdAndLoanId(int customerId, int loanId);
	
}
