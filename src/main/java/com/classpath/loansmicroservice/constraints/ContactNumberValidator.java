package com.classpath.loansmicroservice.constraints;

import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class ContactNumberValidator implements ConstraintValidator<ContactNumber, String> {

    @Override
    public void initialize(ContactNumber constraintAnnotation) {
        // do nothing
    }

    @Override
    public boolean isValid(String contactNumber, ConstraintValidatorContext context) {
        log.info("Came inside the validator method ");
        if (contactNumber == null) {
            return false;
        }
        boolean flag = (contactNumber.length() > 10 && contactNumber.length() < 14) ? true : false;
       log.info(" Flag is {}", flag);
        return flag;
    }
}