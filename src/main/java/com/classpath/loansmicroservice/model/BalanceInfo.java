package com.classpath.loansmicroservice.model;

public class BalanceInfo {
	
	private int accountid;
	private int customerId;
	private int creditScore;
	private String customerName;
	private double balance;
	private String email;
	private String mobileNo;
	
	public BalanceInfo() {
		// TODO Auto-generated constructor stub
	}
	
	public BalanceInfo(int accountid,int customerId,int creditScore, String customerName, double balance, String email, String mobileNo) {
		super();
		this.accountid = accountid;
		this.customerId = customerId;
		this.creditScore = creditScore;
		this.customerName = customerName;
		this.balance = balance;
		this.email = email;
		this.mobileNo = mobileNo;
	}

	public int getAccountid() {
		return accountid;
	}

	public void setAccountid(int accountid) {
		this.accountid = accountid;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public int getCreditScore() {
		return creditScore;
	}

	public void setCreditScore(int creditScore) {
		this.creditScore = creditScore;
	}
}
