package com.classpath.loansmicroservice.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.classpath.loansmicroservice.model.BalanceInfo;
import com.classpath.loansmicroservice.model.Loan;
import com.classpath.loansmicroservice.model.Notification;
import com.classpath.loansmicroservice.repo.LoanRepository;
import com.classpath.loansmicroservice.util.LoanUtilities;
import com.classpath.loansmicroservice.util.Status;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class LoanService {

	@Autowired
	private final LoanRepository loanRepository;

	public Loan createLoan(Loan loan) {

		Loan loanResponse;
		String notificationMessage;
		ResponseEntity<BalanceInfo> customerResponseEntity = LoanUtilities.getBalanceInfo(loan.getAccountId());

		if (Status.NEW.equals(loan.getStatus()) && customerResponseEntity != null
				&& customerResponseEntity.getBody() != null && customerResponseEntity.getBody().getBalance() >= 200000
				&& customerResponseEntity.getBody().getCreditScore() > 800) {

			loan.setCustomerId(customerResponseEntity.getBody().getCustomerId());
			loanResponse = loanRepository.save(loan);

			notificationMessage = "Congratulations!! You have applied loan successfully! Your loan status is: " + loan.getStatus();
			

		} else {
			loanResponse = new Loan();
			log.info("You are not eligible customer or loan status is not NEW");
			notificationMessage = "Sorry for the inconvinience! You are not eligible a customer";
		}
		
		Notification notification = new Notification(customerResponseEntity.getBody().getCustomerId(),
				customerResponseEntity.getBody().getEmail(), notificationMessage,
				customerResponseEntity.getBody().getMobileNo());
		
		LoanUtilities.sendNotification(notification);
		
		return loanResponse;
	}

	public List<Loan> getAllLoans() {
		return (List<Loan>) loanRepository.findAll();
	}

	public Optional<Loan> getLoan(int loanId) {
		return loanRepository.findById(loanId);
	}

	public List<Loan> getLoansByCustomerId(int customerId) {
		List<Loan> loans = loanRepository.findByCustomerId(customerId);
		if (loans.isEmpty()) {
			log.info("No loans found for provided customerId: " + customerId);
		}
		return loans;
	}

	public Loan getLoanByCustomerId(int customerId, int loanId) {

		return loanRepository.findByCustomerIdAndLoanId(customerId, loanId);
	}

	public Loan updateLoanStatus(Loan loan) {

		String notificationMessage;
		
		ResponseEntity<BalanceInfo> customerResponseEntity = LoanUtilities.getBalanceInfo(loan.getAccountId());

		loan.setCustomerId(customerResponseEntity.getBody().getCustomerId());

		Loan matchingLoan = loanRepository.findByCustomerIdAndLoanId(loan.getCustomerId(), loan.getLoanId());

		if (Status.APPROVED.equals(loan.getStatus()) && Status.NEW.equals(matchingLoan.getStatus())) {
			matchingLoan.setBalanceAmount(loan.getLoanAmount());
			matchingLoan.setBalanceTenure(loan.getLoanTenure());
			notificationMessage = "Loan is " + loan.getStatus() + " successfully for customer id " + loan.getCustomerId();
		} else {
			log.info("Loan status should be APPROVED");
			notificationMessage = "Loan status " + loan.getStatus() + " is not valid! Please enter status as APPROVED";
		}
		matchingLoan.setStatus(loan.getStatus());

		Notification notification = new Notification(customerResponseEntity.getBody().getCustomerId(),
				customerResponseEntity.getBody().getEmail(), notificationMessage + loan.getStatus(),
				customerResponseEntity.getBody().getMobileNo());

		LoanUtilities.sendNotification(notification);

		return loanRepository.save(matchingLoan);
	}

	public Loan updateLoanDetails(Loan loan, int customerId, int loanId) {

		String loanPaidMessage;
		Loan loanResponse;

		ResponseEntity<BalanceInfo> customerResponseEntity = LoanUtilities.getBalanceInfo(loan.getAccountId());
		Loan matchingLoan = loanRepository.findByCustomerIdAndLoanId(customerResponseEntity.getBody().getCustomerId(),
				loanId);

		if (Status.APPROVED.equals(matchingLoan.getStatus())) {
			matchingLoan.setBalanceAmount(matchingLoan.getBalanceAmount().subtract(loan.getAmount()));
			matchingLoan.setBalanceTenure(matchingLoan.getBalanceTenure() - 1);
			matchingLoan.setAmount(loan.getAmount());

			loanPaidMessage = "Congratulations!! Your loan EMI Rs. " + loan.getAmount()
					+ " paid successfully. Your remaining loan balance amount is Rs.  " + matchingLoan.getBalanceAmount();

			if (matchingLoan.getBalanceAmount().intValue() <= 0 || matchingLoan.getBalanceTenure() <= 0) {
				matchingLoan.setStatus(Status.CLOSED);
				matchingLoan.setBalanceTenure(0);
				matchingLoan.setLoanTenure(0);
				matchingLoan.setBalanceAmount(new BigDecimal(0));

				loanPaidMessage = "Congratulations!! Your Loan is: " + matchingLoan.getStatus();

			}

			loanResponse = loanRepository.save(matchingLoan);

		} else {
			log.info("Your Loan is either CLOSED or not ACTIVE/APPROVED");
			loanResponse = new Loan();
			loanPaidMessage = "Your Loan is either CLOSED or not ACTIVE/APPROVED. Please connect with Bank.";
		}

		Notification notification = new Notification(customerResponseEntity.getBody().getCustomerId(),
				customerResponseEntity.getBody().getEmail(), loanPaidMessage,
				customerResponseEntity.getBody().getMobileNo());
		
		LoanUtilities.sendNotification(notification);
		
		return loanResponse;
	}

}
